def get_numbers():
    i = 0
    number = 0
    my_list = []
    print "Type 10 numbers between 0 and 1000."
    while i < 10:
        try:
            number = int(raw_input("Enter a number within the range: "))
        except ValueError:
            print "Oopsy! The input is not number." + '\n' + "Try again..."
        if 0 <= number <= 10000:
            i = i+1
            my_list.append (number)
        else:
            print "Oopsy! The input is not within range.." + '\n' + "Try again..."
    return my_list

def sort(list):
    for i in range(1, len(list)):
        j = i-1
        key = list[i]
        while (list[j] > key) and (j >= 0):
           list[j+1] = list[j]
           j = j-1
        list[j+1] = key
    return list

def main():
    unsorted_list = get_numbers()
    print "The numbers you gave:" + '\n' , unsorted_list
    sorted_list = sort(unsorted_list)
    print "The numbers you gave ordered by ascending:" + '\n' , sorted_list

if __name__ == "__main__":
    main()
