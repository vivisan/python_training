import string

max_key = 25
min_key = 0

def get_mode():
    while True:
        print "Choose mode! Enter <e> to encrypt or <d> to decrypt your message"
        mode = raw_input().lower()
        if mode in 'e d'.split():
            return mode
        else:
            print "The input is not correct..."

def get_message():
    print "Enter your message:"
    return raw_input()

def get_shift_value():
    key = -1
    while True:
        try:
            print "Choose a shift value between",min_key, "and" , max_key, ":"
            key = int(raw_input())
            if min_key <= key <= max_key:
                return key
            else:
                print "The input is not in range.." + '\n' + "Try again..."
        except ValueError:
            print "The input is not number.." + '\n' + "Try again..."

def get_final_message(mode,key,message):
    if mode[0] == "e" :
        key = -key
    abc = string.ascii_lowercase
    shifted_abc = abc[key:] + abc[:key]
    ABC = string.ascii_uppercase
    SHIFTED_ABC = ABC[key:] + ABC[:key]
    final_message = ''
    for char in message:
        if char.isalpha():
            if char.islower():
                index = abc.index(char)
                plus_char = shifted_abc[index]
            elif char.isupper():
                index = ABC.index(char)
                plus_char = SHIFTED_ABC[index]
        else:
            plus_char = char
        final_message += plus_char
    return final_message

mode = get_mode()
key = get_shift_value()
message = get_message()
final_message = get_final_message(mode,key,message)
print final_message
